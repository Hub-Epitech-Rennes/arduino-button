int ledPin = 8;                // LED connected to digital pin 8
int buttonPin = 7;             // Button connected to digital pin 7
int ledState = LOW;            // Initial LED state
int buttonPrev = LOW;          // Initial button state

void setup()
{
  pinMode(ledPin, OUTPUT);      // sets the digital pin as output
  pinMode(buttonPin, INPUT);    // sets the digital pin as input
}

void loop()
{
  int val = digitalRead(buttonPin);
  // Ecrivez votre code ici
}
